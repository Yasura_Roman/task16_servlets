package com.yasiuraroman.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PizzeriaList {

    private static PizzeriaList instance = null;
    private List<Pizzeria> pizzeriaList;

    private PizzeriaList(){
        pizzeriaList = new ArrayList<>();
        PizzaList pizzaList = PizzaList.getInstance();
        Pizzeria pizzeria;
        Map<Pizza, Integer> price = new HashMap<>();

        // Pizzeria First
        price.put(pizzaList.getPizzaList().get(0),100);
        price.put(pizzaList.getPizzaList().get(1),120);
        price.put(pizzaList.getPizzaList().get(2),120);
        pizzeria = new Pizzeria("First","12345","Franka 22",price);
        pizzeriaList.add(pizzeria);
        price.clear();

        // Pizzeria Second
        price.put(pizzaList.getPizzaList().get(4),100);
        price.put(pizzaList.getPizzaList().get(1),110);
        price.put(pizzaList.getPizzaList().get(3),120);
        pizzeria = new Pizzeria("Second","24680","Gruda 2",price);
        pizzeriaList.add(pizzeria);
        price.clear();

        // Pizzeria Third
        price.put(pizzaList.getPizzaList().get(0),100);
        price.put(pizzaList.getPizzaList().get(3),120);
        price.put(pizzaList.getPizzaList().get(2),90);
        pizzeria = new Pizzeria("Third","98765","Sugar 18",price);
        pizzeriaList.add(pizzeria);
        price.clear();

        // Pizzeria Fourth
        price.put(pizzaList.getPizzaList().get(0),100);
        price.put(pizzaList.getPizzaList().get(2),150);
        price.put(pizzaList.getPizzaList().get(4),170);
        pizzeria = new Pizzeria("Fourth","19735","Voczal 17",price);
        pizzeriaList.add(pizzeria);
        price.clear();

    }

    public static PizzeriaList getInstance(){
        if(instance == null){
            synchronized (PizzeriaList.class){
                if(instance == null){
                    instance = new PizzeriaList();
                }
            }
        }
        return instance;
    }

    public void setPizzeriaList(List<Pizzeria> pizzeriaList) {
        this.pizzeriaList = pizzeriaList;
    }

    public void addPizzeria(Pizzeria pizzeria) {
        this.pizzeriaList.add(pizzeria);
    }

    public void deletePizzeria(Pizzeria pizzeria) {
        this.pizzeriaList.remove(pizzeria);
    }

    public List<Pizzeria> getPizzeriaList() {
        return pizzeriaList;
    }

    public Map<Pizza, Integer> getRandomPizzaToPrice(){
        PizzaList pizzaList = PizzaList.getInstance();
        Map<Pizza, Integer> price = new HashMap<>();
        //TODO random
        price.put(pizzaList.getPizzaList().get(0),100);
        price.put(pizzaList.getPizzaList().get(1),120);
        price.put(pizzaList.getPizzaList().get(2),120);

        return price;
    }

}
