package com.yasiuraroman.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PizzaList {

    private static PizzaList instance = null;
    private List<Pizza> pizzaList;

    private PizzaList(){
        pizzaList = new ArrayList<>();
        Pizza pizza;
        Map<String,Integer> ingredients = new HashMap<>();

        //pizza Home
        ingredients.put("Цибуля",2);
        ingredients.put("Помідор",2);
        ingredients.put("Перець",2);
        ingredients.put("Ковбаса копчена",2);
        ingredients.put("Сир твердий",2);
        pizza = new Pizza("Home",ingredients,"marinara",PizzaSize.LARGE);
        pizzaList.add(pizza);
        ingredients = new HashMap<>();

        //pizza Ogur
        ingredients.put("Помідор",2);
        ingredients.put("Огірки солоні",2);
        ingredients.put("Ковбаса копчена",2);
        ingredients.put("Сир твердий",2);
        pizza = new Pizza("Ogur",ingredients,"mazic",PizzaSize.LARGE);
        pizzaList.add(pizza);
        ingredients = new HashMap<>();

        //pizza Chic
        ingredients.put("Філе курки",2);
        ingredients.put("Помідор",2);
        ingredients.put("Шампіньйони",2);
        ingredients.put("Сир твердий",2);
        pizza = new Pizza("Chic",ingredients,"tartar",PizzaSize.LARGE);
        pizzaList.add(pizza);
        ingredients = new HashMap<>();

        //pizza Spring
        ingredients.put("Цибуля",2);
        ingredients.put("Помідор",2);
        ingredients.put("Перець",2);
        ingredients.put("Ковбаса копчена",2);
        ingredients.put("Петрушка",2);
        ingredients.put("Гриби",2);
        pizza = new Pizza("Spring",ingredients,"marinara",PizzaSize.LARGE);
        pizzaList.add(pizza);
        ingredients = new HashMap<>();

        //pizza Hava
        ingredients.put("Ананас",2);
        ingredients.put("Курятини",2);
        ingredients.put("Кукуруза",2);
        ingredients.put("Ковбаса копчена",2);
        ingredients.put("Сир твердий",2);
        pizza = new Pizza("Hava",ingredients,"marinara",PizzaSize.LARGE);
        pizzaList.add(pizza);

    }

    public static PizzaList getInstance(){
        if(instance == null){
            synchronized (PizzaList.class){
                if(instance == null){
                    instance = new PizzaList();
                }
            }
        }
        return instance;
    }

    public List<Pizza> getPizzaList() {
        return pizzaList;
    }

    public void setPizzaList(List<Pizza> pizzaList) {
        this.pizzaList = pizzaList;
    }

    public void addPizza(Pizza pizza) {
        this.pizzaList.add(pizza);
    }

    public void deletePizza(Pizza pizza) {
        this.pizzaList.remove(pizza);
    }

}
