package com.yasiuraroman.model;

import java.util.List;

public class Order {

    private Pizzeria pizzeria;
    private List<Pizza> pizzas;
    private String owner;

    public Order(Pizzeria pizzeria, List<Pizza> pizzas, String owner) {
        this.pizzeria = pizzeria;
        this.pizzas = pizzas;
        this.owner = owner;
    }

    public Pizzeria getPizzeria() {
        return pizzeria;
    }

    public void setPizzeria(Pizzeria pizzeria) {
        this.pizzeria = pizzeria;
    }

    public List<Pizza> getPizza() {
        return pizzas;
    }

    public void setPizza(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public void addPizza(Pizza pizza) {
        this.pizzas.add(pizza);
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getTotalPrice() {
        int result = 0;
        for (Pizza piz: pizzeria.getPizzaToPrice().keySet()
             ) {
            result += pizzas.stream()
                    .filter(e -> e.equals(piz))
                    .mapToInt(e -> pizzeria.getPizzaToPrice().get(e))
                    .sum();
        }
        return result;
    }

}
