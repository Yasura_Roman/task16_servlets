package com.yasiuraroman.model;

import java.util.ArrayList;
import java.util.List;

public class OrderList {
    private static OrderList instance = null;
    private List<Order> orderList;

    private OrderList(){
        orderList = new ArrayList<>();
    }

    public static OrderList getInstance(){
        if(instance == null){
            synchronized (OrderList.class){
                if(instance == null){
                    instance = new OrderList();
                }
            }
        }
        return instance;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public void addOrder(Order order) {
        this.orderList.add(order);
    }

    public void deleteOrder(Order order) {
        this.orderList.remove(order);
    }

}
