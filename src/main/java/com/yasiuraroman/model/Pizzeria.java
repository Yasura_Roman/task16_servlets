package com.yasiuraroman.model;

import java.util.Map;

public class Pizzeria {

    private String name;
    private String phoneNumber;
    private String address;
    private Map<Pizza,Integer> pizzaToPrice;

    public Pizzeria(String name, String phoneNumber, String address, Map<Pizza, Integer> pizzaToPrice) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.pizzaToPrice = pizzaToPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Map<Pizza, Integer> getPizzaToPrice() {
        return pizzaToPrice;
    }

    public void setPizzaToPrice(Map<Pizza, Integer> pizzaToPrice) {
        this.pizzaToPrice = pizzaToPrice;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
