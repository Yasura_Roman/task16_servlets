package com.yasiuraroman.controller;

import com.yasiuraroman.model.Order;
import com.yasiuraroman.model.OrderList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/orders")
public class OrderController extends HttpServlet {

    private Logger logger = LogManager.getLogger();
    private OrderList orderList = OrderList.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out =  resp.getWriter();
        out.println("<html><body>");
        //Навігація
        out.println("<p> <a href='pizzerias'>Pizzerias</a> </p>");
        out.println("<p> <a href='pizzas'>Pizzas</a> </p>");
        out.println("<p> <a href='orders'>Orders</a> </p>");

        out.println("<H2>List of order </H2>");
        for (Order order:orderList.getOrderList()
        ) {
            out.println("<p>"
                    + " owner = " + order.getOwner()
                    + " pizzeria = " + order.getPizzeria()
                    + " pizza = " + order.getPizza()
                    + " total price = " + order.getTotalPrice()
                    + "</p>");
        }

        out.println("</body></html>");
    }


    //TODO
}
