package com.yasiuraroman.controller;

import com.yasiuraroman.model.Pizzeria;
import com.yasiuraroman.model.PizzeriaList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/pizzerias")
public class PizzeriaController extends HttpServlet {

    private Logger logger = LogManager.getLogger();
    private PizzeriaList pizzeriaList = PizzeriaList.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out =  resp.getWriter();
        out.println("<html><body>");
        //Навігація
        out.println("<p> <a href='pizzerias'>Pizzerias</a> </p>");
        out.println("<p> <a href='pizzas'>Pizzas</a> </p>");
        out.println("<p> <a href='orders'>Orders</a> </p>");

        out.println("<H2>List of pizzeria </H2>");
        for (Pizzeria pizzeria:pizzeriaList.getPizzeriaList()
             ) {
            out.println("<p>" + pizzeria.getName() + " ("
                    + " address = " + pizzeria.getAddress()
                    + " phone = " + pizzeria.getPhoneNumber()
                    + ")" + "</p>");
        }

        //Формочки
        out.println("<form action='pizzerias' method='POST'>" +
                " Name: <input type='text' name='pizzeria_name'>\n" +
                " Address: <input type='text' name='pizzeria_address'>\n" +
                " Phone: <input type='text' name='pizzeria_phone'>\n" +
                " <button type='submit'>Add pizzeria</button>" +
                "</form>");
        out.println("<form>" +
                "<p><b>Delete</b></p>" +
                "<p> Pizzeria name: <input type='text' name='pizzeria_delete_name'>" +
                "   <input type='button' onclick='remove(this.form.pizzeria_delete_name.value)' name='pizzeria_delete_button'>" +//дописати name=
                "</p>" +
                "</form>");
        out.println("<script type='text/javascript'>" +
                " function remove(name) { fetch('pizzerias/' + name, {method: 'DELETE'}); }" +
                "</script>");

        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newPizzeriaName = req.getParameter("pizzeria_name");
        String newPizzeriaAddress = req.getParameter("pizzeria_address");
        String newPizzeriaPhone = req.getParameter("pizzeria_phone");
        Pizzeria newPizzeria = new Pizzeria(newPizzeriaName,newPizzeriaPhone,newPizzeriaAddress,pizzeriaList.getRandomPizzaToPrice());
        pizzeriaList.addPizzeria(newPizzeria);
        doGet(req,resp);
    }

//    @Override
//    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String name = req.getRequestURI();
//    }

    //TODO
}
