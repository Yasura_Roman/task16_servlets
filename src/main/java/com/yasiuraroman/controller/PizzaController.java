package com.yasiuraroman.controller;

import com.yasiuraroman.model.Pizza;
import com.yasiuraroman.model.PizzaList;
import com.yasiuraroman.model.Pizzeria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/pizzas")
public class PizzaController extends HttpServlet {

    private Logger logger = LogManager.getLogger();
    private PizzaList pizzaList = PizzaList.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out =  resp.getWriter();
        out.println("<html><body>");
        //Навігація
        out.println("<p> <a href='pizzerias'>Pizzerias</a> </p>");
        out.println("<p> <a href='pizzas'>Pizzas</a> </p>");
        out.println("<p> <a href='orders'>Orders</a> </p>");

        out.println("<p>pizzaList</p>");
        String ingredients;
        for (Pizza pizza:pizzaList.getPizzaList()
        ) {
            ingredients = pizza.getIngredients().entrySet()
                    .stream()
                    .map(e -> e.getKey() + "  " + e.getValue())
                    .reduce( (e1,e2) -> e1 + "\n" + e2)
                    .get();
            out.println("<p>" + pizza.getName() + " ("
                    + " sauce = " + pizza.getSauce()
                    + " size = " + pizza.getSize()
                    + " ingredients = " + ingredients
                    + ")" + "</p>");
        }
        out.println("</body></html>");
    }
}
